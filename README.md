# 0trust

deployment of 0trust blockchain services. follwoing figure discussed the architecture 
of the 0trust platfrom. 

![Alt text](0trust-architecture.png?raw=true "0trust platfrom architecture")

## services

seven main servies

```
1. elassandra
2. redis
3. aplos
4. gateway
5. groop
6. kibana
7. nginx
```

## configuration

change `host.docker.local` field in `.env` file to local machines ip. also
its possible to add a host entry to `/etc/hosts` file by overriding
`host.docker.local` with local machines ip. following is an example of
`/etc/hosts` file.

```
10.4.1.104    host.docker.local
```

give write permission to `/private/var/services/connect/elassandra` directory
in the server. following is the way to give the permission

```
sudo mkdir /private/var/services/connect/elassandra
sudo chmo -R 777 /private
```

## deploy services

Start services in following order

```
docker-compose up -d elassandra
docker-compose up -d redis
docker-compose up -d aplos
docker-compose up -d gateway
docker-compose up -d nginx // optional to deploy
docker-compose up -d groop // optionsal to deploy
docker-compose up -d kibana // optional to deploy
```

## connect apis

gateway service will start a REST api on `7654` port. for an example if your
machines ip is `10.4.1.104` the apis can be access via `10.4.1.104:7654/api/<apiname>`.
following are the available rest api end points and their specifications

#### 1. create score

```
# request
curl -XPOST "http://34.135.203.85:7654/api/scores" \
--header "Content-Type: application/json" \
--header "Bearer: eyJkaWdzaWciOiJORWpYZnIwQjJMZG4ySGxPb2t5blp0dkNzSFVqMGFoVTVZd1F5TmJSVCtOYjlwTnBXcEsvUi9UbDZpanhPVVJiVlJHc2NHaFIrcWVCbkZhK09YYjBmMGlacVh0WHBDVXV6bnJOcTFKRmpGZC8zSU80L1o4SXl3WG1EdWFGcUg5Njc5VE9neVRkcU1nT01VeWNNWTF0bmtIUStWVUtUN0JTV0NWMEM3ZmNXbEE9IiwiaWQiOiJlcmFuZ2FlYkBnbWFpbC5jb20iLCJpc3N1ZVRpbWUiOjE1NTg0ODk4ODksInJvbGVzIjoiIiwidHRsIjo2MH0=" \
--data '
{
  "id": "111110",
  "execer": "admin:admin",
  "messageType": "addScore",
  "digsig": "",
  "userId": "aaa",
  "numerator": 4.12,
  "denominator": 3.25
}
'

# reply
{"code":200,"msg":"score added"}
```

#### 2. create scores

```
# request
curl -XPOST "http://34.135.203.85:7654/api/scores" \
--header "Content-Type: application/json" \
--header "Bearer: eyJkaWdzaWciOiJORWpYZnIwQjJMZG4ySGxPb2t5blp0dkNzSFVqMGFoVTVZd1F5TmJSVCtOYjlwTnBXcEsvUi9UbDZpanhPVVJiVlJHc2NHaFIrcWVCbkZhK09YYjBmMGlacVh0WHBDVXV6bnJOcTFKRmpGZC8zSU80L1o4SXl3WG1EdWFGcUg5Njc5VE9neVRkcU1nT01VeWNNWTF0bmtIUStWVUtUN0JTV0NWMEM3ZmNXbEE9IiwiaWQiOiJlcmFuZ2FlYkBnbWFpbC5jb20iLCJpc3N1ZVRpbWUiOjE1NTg0ODk4ODksInJvbGVzIjoiIiwidHRsIjo2MH0=" \
--data '
[
  {
    "id": "0101",
    "execer": "admin:admin",
    "messageType": "addScore",
    "digsig": "",
    "userId": "aaa",
    "numerator": 24.12,
    "denominator": 3.25
  },
  {
    "id": "0202",
    "execer": "admin:admin",
    "messageType": "addScore",
    "digsig": "",
    "userId": "aac",
    "numerator": 1.22,
    "denominator": 6.25
  },
  {
    "id": "0303",
    "execer": "admin:admin",
    "messageType": "addScore",
    "digsig": "",
    "userId": "aad",
    "numerator": 3.12,
    "denominator": 9.25
  }
]
'

# reply
{"code":200,"msg":"scores added"}
```


#### 3. get score 

```
# request
curl -XPOST "http://34.135.203.85:7654/api/scores" \
--header "Content-Type: application/json" \
--header "Bearer: eyJkaWdzaWciOiJORWpYZnIwQjJMZG4ySGxPb2t5blp0dkNzSFVqMGFoVTVZd1F5TmJSVCtOYjlwTnBXcEsvUi9UbDZpanhPVVJiVlJHc2NHaFIrcWVCbkZhK09YYjBmMGlacVh0WHBDVXV6bnJOcTFKRmpGZC8zSU80L1o4SXl3WG1EdWFGcUg5Njc5VE9neVRkcU1nT01VeWNNWTF0bmtIUStWVUtUN0JTV0NWMEM3ZmNXbEE9IiwiaWQiOiJlcmFuZ2FlYkBnbWFpbC5jb20iLCJpc3N1ZVRpbWUiOjE1NTg0ODk4ODksInJvbGVzIjoiIiwidHRsIjo2MH0=" \
--data '
{
  "id": "1111xx",
  "execer": "admin:admin",
  "messageType": "getScore",
  "digsig": "",
  "userId": "aaa"
}
'

# reply
{"numerator":5.1111,"denominator":7.1332,"timestamp":"2021-07-12 20:55:21.308","userId":"aaa"}
```
